import Square from './square'

type itx = -1|0|1;
interface RowProps {
  ridx:number, 
  p0:itx, 
  p1:itx, 
  p2:itx,
  onChange(idx:number):void,
};

function BoardRow(props:RowProps) {
  return (
    <div className="board-row">
      <Square idx={props.ridx} value={props.p0} onChange={props.onChange}/>
      <Square idx={props.ridx+1} value={props.p1} onChange={props.onChange}/>
      <Square idx={props.ridx+2} value={props.p2} onChange={props.onChange}/>
    </div>
  );
}
interface BoardProps {
  squares: number[],
  nextPlayer: number,
  onChange(idx: number): void,
}
function Board(props:BoardProps) {

  // Validate that state is in range before type casting.
  for(let i of props.squares) if(i > 1 || i < -1) throw Error("Out-of-range state");
  const squares = props.squares as itx[];

  return (
    <div>
      <BoardRow ridx={0} p0={squares[0]}
        p1={squares[1]} p2={squares[2]} onChange={props.onChange}/>
      <BoardRow ridx={3} p0={squares[3]}
        p1={squares[4]} p2={squares[5]} onChange={props.onChange}/>
      <BoardRow ridx={6} p0={squares[6]}
        p1={squares[7]} p2={squares[8]} onChange={props.onChange}/>
    </div>
  );
}

export default Board;