import Board from "./board"
import {useState} from "react"

let calculateWinner = (squares:number[]): number => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for(let line of lines) {
    const[a,b,c] = line;
    if (squares[a] && squares[a] === squares[b] 
      && squares[a] === squares[c]) return squares[a];
  }
  return 0;
}

function Game(props:{}) {
  const [state, setState] = useState({
    // All elements are [-1, 1], and is a 3x3 grid.
    state:Array(9).fill(0),
    // Elements are [0,8], and the i'th position indicates the i'th move.
    // Player is inferred to be 1 at the start.
    history: Array(9).fill(0), 
    nextPlayer:1, time:0})
  const winner = calculateWinner(state.state);

  let apply_changes = (input:number[], maxIdx:number): number[] => {
    let out = Array(9).fill(0);
    let nextPlayer = 1;
    for(let i = 0; i < maxIdx; i++) {
      out[input[i]] = nextPlayer;
      nextPlayer *= -1;
    }
    return out;
  }

  let handleChange = (idx:number) => {
    let newState = {...state};
    let newHistory = state.history.slice();
    let newBoardState = state.state.slice();
    if(state.state[idx] === 0 && winner===0) {
      newBoardState[idx] = state.nextPlayer
      newState.nextPlayer *= -1;
      newHistory[state.time] = idx; 
      newState.state = newBoardState;
      newState.history = newHistory;
      newState.time += 1
      setState(newState)
    }
  }

  let status;
  if(winner === 0) status = "Next player is " + (state.nextPlayer === 1 ? "O" : "X");
  else if(winner === 1) status = "O wins!";
  else status = "X wins!";
  let jumpTo = (idx: number) => {
    let newState = {...state};
    let newBoardState = apply_changes(state.history, idx);
    newState.time = idx;
    newState.state = newBoardState;
    newState.nextPlayer = (-1) ** (idx);
    setState(newState)
  }

  const arr = Array(state.time).fill(0);
  const moves = arr.map((value, index) => {
    const desc = index ? 'Go to move #' + index : 'Go to game start';
    return (
      <li key={index}>
        <button onClick={() => jumpTo(index)}>{desc}</button> 
      </li>      
      );    
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board squares={state.state} nextPlayer={state.nextPlayer} onChange={handleChange}/>
      </div>
      <div className="game-info">
        <div>{status}</div>
        <ol>{moves}</ol>
      </div>
    </div>
  ); 
}

export default Game;