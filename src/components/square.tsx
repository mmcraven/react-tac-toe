interface SquareProps{
  value:0|1|-1,
  idx:number,
  onChange(idx:number):void,
}
function Square(props:SquareProps) {
  let str = (value:number) => {
    switch(value) {
      case -1: return "x";
      case 0: return "";
      case 1: return "o";
      default: throw Error("Not a valid state");
    }
  }
  const key = props.idx;
  return (
    <button key={key} className="square" onClick={()=>{
      props.onChange(key);}}>
      {str(props.value)}
    </button>
  );
}


export default Square;